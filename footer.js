import React from 'react';

class Footer extends React.Component {

    render() {
        return ( <
            div >




            <
            footer className = "bg-light text-center text-lg-start" >

            <
            div className = "container p-4" >

            <
            div className = "row" >

            <
            div className = "col-lg-3 col-md-6 mb-4 mb-md-0" >
            <
            h5 className = "text-uppercase" > EXPLORE < /h5>

            <
            ul className = "list-unstyled mb-0" >
            <
            li >
            <
            a href = "#!"
            className = "text-dark" > What We Do < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > Funding < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > GoFundMe < /a> <
            /li>

            <
            /ul> <
            /div>

            <
            div className = "col-lg-3 col-md-6 mb-4 mb-md-0" >
            <
            h5 className = "text-uppercase mb-0" > ABOUT < /h5>

            <
            ul className = "list-unstyled" >
            <
            li >
            <
            a href = "#!"
            className = "text-dark" > About Us < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > Blog < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > Trust & Safety < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > Help & Support < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > Press < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > Careers < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > Contact < /a> <
            /li> <
            /ul> <
            /div>



            <
            div className = "col-lg-3 col-md-6 mb-4 mb-md-0" >
            <
            h5 className = "text-uppercase" > ENTREPRENEURS < /h5>

            <
            ul className = "list-unstyled mb-0" >
            <
            li >
            <
            a href = "#!"
            className = "text-dark" > How It Works < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > Indiegogo vs.Kickstarter < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > Education Center < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > Experts Directory < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > Fees < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > Enterprise < /a> <
            /li> <
            li >
            <
            a href = "#!"
            className = "text-dark" > China < /a> <
            /li> <
            /ul> <
            /div>



            <
            div className = "col-lg-3 col-md-6 mb-4 mb-md-0" >
            <
            h5 className = "text-uppercase mb-0" > Find it first on Indiegogo < /h5>

            <
            ul className = "list-unstyled" >
            <
            li >
            <
            p > Discover new and clever products in the Indiegogo newsletter < /p> <
            /li> <
            li >
            <
            div className = "container p-4 pb-0"
            style = {
                { height: '50px', width: '100px' } } >

            <
            section className = "" >
            <
            form action = "" >



            <
            div >


            <
            div className = "form-outline form-white mb-4"
            style = {
                { height: '50px', width: '250px' } } >
            <
            strong > Sign up
            for our newsletter < /strong>   <
            input type = "email"
            id = "form5Example2"
            className = "form-control" / >
            <
            label className = "form-label"
            for = "form5Example2" > Email address < /label>  <
            button type = "submit"
            className = "btn btn-outline-light mb-4" >
            Sign up <
            /button>

            <
            /div> <
            /div>



            <
            /form> <
            /section>

            <
            /div>

            <
            /li>

            <
            /ul> <
            /div>

            <
            /div>

            <
            /div>

            <
            /footer>



            <
            /div>


        )
    }
}
export default Footer
